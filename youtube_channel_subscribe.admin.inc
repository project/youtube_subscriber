<?php
/**
 * @file
 * Functionality for the admin views.
 */

/**
 * Form constructor for the admin form.
 */
function youtube_channel_subscribe_admin() {
  $form = array();
  $form['youtube'] = array(
    '#type' => 'fieldset',
    '#description' => t('Please Insert Youtube channel name or channel id in bellow field</br>
      You can use token [youtube:button] anywhere in body'),
  );
  $form['youtube']['youtube_channel_subscribe_channel_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Youtube Channel Name'),
    '#description' => t('Enter your youtube channel name'),
    '#default_value' => variable_get('youtube_channel_subscribe_channel_name'),
  );
  $form['youtube']['youtube_channel_subscribe_channel_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Youtube Channel Id'),
    '#description' => t('Enter your youtube channel id'),
    '#default_value' => variable_get('youtube_channel_subscribe_channel_id'),
  );
  return system_settings_form($form);
}
