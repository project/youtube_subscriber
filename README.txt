
CONTENTS OF THIS FILE
---------------------
 * Description
 * Introduction
 * Installing
 * Uninstalling
 * Frequently Asked Questions (FAQ)
 * Known Issues
 * More Information


DESCRIPTION
-----------
YouTube channel subscribe button using token.
You can place token [youtube:button] anywhere in body.
You can easily place YouTube subscribe button without any code. 
Just need to add token to your body and also set channel name or channel id in
admin configuration page.

INTRODUCTION
------------

Current Maintainer: Ajay Gadhavana <https://www.drupal.org/u/ajay-gadhavana>

Youtube channel subscribe button using token. You can place token 
[youtube:button] anywhere in body 

INSTALLING
----------

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

Once Youtube channel subscribe  module is installed and enabled,
you can adjust the settings for your
site's at admin/config/media/youtube


UNINSTALLING
------------

You can easily uninstall module from module list, there is no any dependency


FREQUENTLY ASKED QUESTIONS (FAQ)
--------------------------------

- There are no frequently asked questions at this time.


KNOWN ISSUES
------------

Error - Error at the place of button
Solution :  Please confirm that you placed right channel name 
or channel id in configuration page


MORE INFORMATION
----------------

- There are no more information.
